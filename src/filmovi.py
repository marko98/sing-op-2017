from provere import *

def izbor_zanra():
    '''
    Biranje zanra od podstojecih koji se ucitavaju iz fajla.
    '''

    with open("../data/zanrovi.txt", "r") as f:
        linija = f.read()
        zanrovi = eval(linija)

        while True:
            try:
                print("Izaberite jedan od ponudjenih zanrova: ")
                for zanr in range(1, len(zanrovi)+1):
                    print(str(zanr) + " - " + zanrovi[zanr-1])
                izbor = int(input("Molimo izaberite opciju: "))
                if izbor == 0:
                    stanje = True
                    while stanje:
                        izbor = int(input("Molimo izaberite jednu od postojecih opcija: "))
                        if izbor != 0:
                            stanje = False
                zanr = zanrovi[izbor - 1]
                break
            except ValueError:
                print("Molimo unesite broj, koji se nalazi ispred zeljenog zanra.")
            except IndexError:
                print("Molimo unesite jedan od postojecih zanrova.")
        return zanr


def unos_filma():
    '''
    Upisivanje novog filma u listu filmova.
    '''

    film = dict()
    ime = ""
    while ime == "":
        ime = input("Molimo unesite ime filma: ")
        ime = ime.title()
    zanr = izbor_zanra()
    duzina = unos_broja("Molimo unesite duzinu filma (u minutima): ")
    while duzina <= 60:
        print("Film treba da traje duze od jednog sata.")
        duzina = unos_broja("Molimo unesite duzinu filma, a da bude duza od jednog minuta: ")

    with open("../data/filmovi.txt", "r") as p:
        linija = p.read()
        idfilma = eval(linija)
        idfilma = str(len(idfilma)+1)

    film["id"] = idfilma
    film["ime"] = ime
    film["zanr"] = zanr
    film["duzina"] = str(duzina)

    with open("../data/filmovi.txt", "r") as a:
        linija = a.read()
        filmovi = eval(linija)

    with open("../data/filmovi.txt", "w") as q:
        filmovi.append(film)
        q.write(str(filmovi))
        print("Dodat je novi film.")