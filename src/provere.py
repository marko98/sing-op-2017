def unos_broja(tekst):
    '''
    Funkcija za proveru unosa broja.
    Uglavnom koriscena za navigaciju kroz menije.
    '''
    while True:
        try:
            unos = int(input(tekst))
            return unos
        except ValueError:
            print("Molimo unesite odgovarajuci broj.")

def unos_dinara(tekst):
    '''
    Funkcija za proveru unosa broja.
    Uglavnom koriscena za navigaciju kroz menije.
    '''
    while True:
        try:
            unos = int(input(tekst))
            if unos < 100:
                print("Minimalna cena je 100 dinara.")
            if unos >= 100:
                return unos
        except ValueError:
            print("Molimo unesite odgovarajuci broj.")

def provera_vrednosti(string,x):  #proveravanje da li je uneta vrednost ceo broj
    broj = -1
    while (broj < 0) or (broj > x):
        try:
            broj = int(input(string))
        except ValueError:
            print("Morate uneti dobar broj iz menija!")
    return broj