from provere import *
from _datetime import *
#import datetime
import time

racun = []
konacan_racun = []

def izdavanje_racuna(racun):
    #lista_ispis = []
    #print(racun)
    broj_ispis = 1
    #for j in range(0, len(racun)):
    #    racun2 = racun[j]
    #    lista_ispis.append(racun2)
    #print(racun2)
    #print(lista_ispis)
    print("Trenutno na vasem racunu: ")
    for i in range(0, len(racun)):
        print(str(broj_ispis) + "." + " id projekcije: " + racun[i]["id projekcije"] + "| naziv filma: " + racun[i]["ime_filma"]
              + "| sala: " + racun[i]["sala"] + "| vreme: " + racun[i]["vreme"] + "| datum: " + racun[i]["datum"]
              + "| broj karata: " + racun[i]["broj karata"] + "| cena karte: " + racun[i]["cena karte"]
              + "| ukupna cena: " + str(racun[i]["ukupna cena"]) + " dinara.")
        broj_ispis += 1
    time.sleep(1)
    print()
    print("Da li zelite da:")
    odabir_opcije = provera_vrednosti("""
    1. Odstampate racun
    2. Izbrisete racun
Molimo vas odaberite opciju iz menija: \n""", 2)

    if odabir_opcije == 1:
        pojedinacan_racun = []
        ukupna_cena = 0
        with open("../data/racuni.txt", "r") as r:
            konacan_racun = r.read()
            konacan_racun = eval(konacan_racun)

        #print(konacan_racun)

        with open("../data/projekcije.txt", "r") as r:
            linija = r.read()
            linija = eval(linija)
            #print(linija)
            #print(racun)

            danasnji_datum = str(datetime.today())
            danasnji_datum = danasnji_datum.strip().split()
            datum = danasnji_datum[0].split("-")
            datum = datum[2] + " " + datum[1] + " " + datum[0]
            vreme = danasnji_datum[1].split(":")
            vreme = vreme[0] + " " + vreme[1]
            sifra = len(konacan_racun)
            #print(datum)

            for stavka in racun:
                for projekcija in linija:
                    if stavka["id projekcije"] == projekcija["id"]:
                       with open("../data/projekcije.txt", "w") as q:
                            projekcija["broj_slobodnih_mesta"] = str(int(projekcija["broj_slobodnih_mesta"]) - int(stavka["broj karata"]))
                            q.write(str(linija))
                ukupna_cena = ukupna_cena + stavka["ukupna cena"]
                pojedinacan_racun.append(stavka)

            finalni_racun = dict()
            finalni_racun["sifra"] = str(sifra)
            finalni_racun["vreme"] = vreme
            finalni_racun["datum"] = datum
            finalni_racun["ukupna cena"] = str(ukupna_cena)
            pojedinacan_racun.append(finalni_racun)

            konacan_racun.append(pojedinacan_racun)

            with open("../data/racuni.txt", "w") as w:
                w.write(str(konacan_racun))

            #del pojedinacan_racun[:]


        del racun[:]
        print("Racun je odstampan.")
    elif odabir_opcije == 2:
        del racun[:]
        print("Racun je izbrisan.")
    return racun

def ucitavanje_projekcija():    #ucitavanje projekcija i njihovog sadrzaja iz tekstualnog fajla
    lista_projekcija = []
    with open("../data/projekcije.txt", "r") as f:
        linija = f.read()
        projekcije = eval(linija)
        #print(projekcije)
        for i in range(0,len(projekcije)):
            #print(projekcije[i])
            projekcija = projekcije[i]
            recnik2 = dict()
            recnik2["id"] = projekcija["id"]
            recnik2["ime_filma"] = projekcija["film"]
            recnik2["zanr"] = projekcija["zanr"]
            recnik2["sala"] = projekcija["sala"]
            recnik2["cena_karte"] = projekcija["cena"]
            recnik2["datum"] = projekcija["datum"]
            recnik2["vreme"] = projekcija["vreme"]
            #recnik2["trajanje"] = projekcija["trajanje"]
            recnik2["slobodna_mesta"] = projekcija["broj_slobodnih_mesta"]
            recnik2["obrisano"] = projekcija["vidljivost"]
            lista_projekcija.append(recnik2)
        #print(lista_projekcija)
    return lista_projekcija


def prodaja_karata():  # funkcija za prodavanje karata
    print("""
    1. Prodaja karata
    2. Izdavanje racuna
    0. Povratak na glavni meni""")
    prodaja = provera_vrednosti("Unesite opciju menija: ", 2)

    if prodaja == 1:
        lista_za_prodaju = ucitavanje_projekcija()
        privremena_lista = []
        print("Dostupne projekcije:")
        redni_broj = 1
        #print(lista_za_prodaju)
        for i in range(0, len(lista_za_prodaju)):
            print("\t" + str(redni_broj) + "."+ " naziv filma: " + lista_za_prodaju[i]["ime_filma"] +
                  "| sala: " + lista_za_prodaju[i]["sala"] + "| vreme: " + lista_za_prodaju[i]["vreme"] +
                  "| datum: " + lista_za_prodaju[i]["datum"] + "| broj slobodnih mesta: " +
                   lista_za_prodaju[i]["slobodna_mesta"] +
                  "| cena karte: " + lista_za_prodaju[i]["cena_karte"] + " dinara.")
            privremena_lista.append(lista_za_prodaju[i]["id"])

            redni_broj += 1
        #print(lista_za_prodaju)
        #print(privremena_lista)
        odabir_karte = provera_vrednosti("Unesite redni broj projekcije za koju zelite da prodate kartu: ",
                                           len(lista_za_prodaju))
        if odabir_karte == 0:
            print("Odabrali ste nepostojecu opciju.")
            del racun[:]
            return
        odabir_karte = privremena_lista[odabir_karte - 1]
        #print(lista_za_prodaju)
        #print(int(odabir_karte))
        #print(lista_za_prodaju[int(odabir_karte)-1]["slobodna_mesta"])
        if lista_za_prodaju[int(odabir_karte) - 1]["obrisano"] == False:
            broj_karata = ""
            while broj_karata == "":
                broj_karata = input("Unesite broj karata za prodaju: ")
                try:
                    provera_karata = int(broj_karata)
                    if int(broj_karata) <= 0:
                        print("Broj karata ne moze biti negativan ili jednak 0.")
                        broj_karata = ""
                    if int(broj_karata) > int(lista_za_prodaju[int(odabir_karte)-1]["slobodna_mesta"]):
                        print("Nema toliko slobodnih mesta.")
                        broj_karata = ""
                    if provera_karata == True:
                        break
                except ValueError:
                    print("Broj karata mora biti ceo broj.")
                    broj_karata = ""
            indeks = int(odabir_karte) - 1
            ukupna_cena = float(lista_za_prodaju[indeks]["cena_karte"]) * float(broj_karata)
            #print(ukupna_cena)

            trenutni_racun = dict()
            trenutni_racun["id projekcije"] = lista_za_prodaju[indeks]["id"]
            trenutni_racun["ime_filma"] = lista_za_prodaju[indeks]["ime_filma"]
            trenutni_racun["sala"] = lista_za_prodaju[indeks]["sala"]
            trenutni_racun["vreme"] = lista_za_prodaju[indeks]["vreme"]
            trenutni_racun["datum"] = lista_za_prodaju[indeks]["datum"]
            trenutni_racun["broj karata"] = broj_karata
            trenutni_racun["cena karte"] = lista_za_prodaju[indeks]["cena_karte"]
            trenutni_racun["ukupna cena"] = ukupna_cena

            racun.append(trenutni_racun)
                          #lista_za_prodaju[indeks]["ime_filma"] + ", " + lista_za_prodaju[indeks]["sala"] + ", " +
                          #lista_za_prodaju[indeks]["vreme"] + ", " + lista_za_prodaju[indeks]["datum"] + ", " + broj_karata +
                          #", " + lista_za_prodaju[indeks]["cena_karte"] + ", " + str(ukupna_cena))
            print("Dodato je:", broj_karata, "karte za film", lista_za_prodaju[indeks]["ime_filma"], "na Vas racun.")
            nastavak = provera_vrednosti("""\nDa li zelite da dodate jos karata na racun:
            1. Da
            2. Ne
            Odaberite opciju iz menija: """, 2)
            if nastavak == 1:
                prodaja_karata()

            if nastavak == 2:
                izdavanje_racuna(racun)
        if lista_za_prodaju[int(odabir_karte) - 1]["obrisano"] == True:
            print("Odabrana projekcija nije aktivna.")
            prodaja_karata()

    if prodaja == 2:
        if racun != []:
            izdavanje_racuna(racun)
        if racun == []:
            print("Vas racun je prazan. Kupite karte.")
            prodaja_karata()

    if prodaja == 0:
        del racun[:]