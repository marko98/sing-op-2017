def login(): #funkcija za logovanje
    '''
    Funkcija za login. Pproverava da li se uneseno korisnicko ime i sifra poklapaju sa podacima iz baze.
    Vraca odgovarajucu ulogu korisnika.
    '''

    with open("../data/korisnici.txt", "r") as f:
        linija = f.read()
        korisnici = eval(linija)

        while True: #petlja koja omogucava nasem programu da vrsi nas kod dokle god je nesto True
            print("Upisite svoje korisnicko ime i sifru") #ispisuje ono sto se nalazi u zagradi (stringove ako su pod navodnicima ili vrednosti nekih promenljivih)
            username = input("Username: ") #unosenje korisnickog imena
            password = input("Password: ") #unosenje sifre
            for i in range(len(korisnici)): #for petlja koja omogucava prolazak kroz listu korisnici
                if username == korisnici[i]["korisnickoime"] and password == korisnici[i]["sifra"]:#provera da li se korisnicko ime i sifra podudaraju sa nekim korisnickim imenom i sifrom iz korisnika unutar liste korisnici
                    print("Ulogovani ste kao {} {}.".format(korisnici[i]["ime"], korisnici[i]["prezime"])) #pozivamo vrednosti odredjenih kljuceva u odredjenom recniku da se upisu umesto {} redom
                    return korisnici[i]["uloga"] #odmah vraca vrednost iz f-je
                elif username == "" or password == "":#u slucaju da je za korisnicko ime ili sifru unet prazan string
                    print("Korisnicko ime i/ili sifra ne mogu biti prazni!")
                    break #prekida petlju
            else:#za ostale slucajeve
                print("Korisnicko ime i/ili sifra nisu ispravni!")