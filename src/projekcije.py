import datetime #importujemo iz modula datetime
from provere import *
from filmovi import *


def unesite_datum(): #funkcija za unosenje datuma projekcije
    '''
    Funkcija koja proverava da li je datum unesen u ispravnom formatu
    i proverava da li unesen datum u buducem vremenu.
    '''
    datum_danasnji = datetime.date.today()
    while True: #petlja koja omogucava nasem programu da vrsi nas kod dokle god je nesto True
        try: #try i except - sintaksa blokova za obradu greske
            datum = input("Unesite datum projekcije u formatu dd mm yyyy: ") #unosenje datuma projekcije
            datum = datetime.datetime.strptime(datum, "%d %m %Y").date() #parametru datum dodeljujemo novu vrednost
            if datum < datum_danasnji:
                print("Molimo unesite datum koji nije istekao.")
            if datum == datum_danasnji:
                print("Molimo unesite datum koji nije danasnji.")
            else:
                break #prekida petlju
        except ValueError: #sprecava da program "pukne", vec kaze ako naidjes na odredjenu vrstu greske uradi to sto pise u bloku
            print("Molimo unesite datum projekcije u trazenom formatu.") #ispisuje ono sto se nalazi u zagradi (stringove ako su pod navodnicima ili vrednosti nekih promenljivih)
    return datum #odmah vraca vrednost iz f-je

def unesite_vreme(): #funkcija za unosenje vremena projekcije
    while True: #petlja koja omogucava nasem programu da vrsi nas kod dokle god je nesto True
        try: #try i except - sintaksa blokova za obradu greske
            vreme = input("Unesite vreme projekcije u formatu hh mm: ") #unosenje vremena projekcije
            vreme = datetime.datetime.strptime(vreme, "%H %M").time() #parametru vreme dodeljujemo novu vrednost
            break #prekida petlju
        except ValueError: #sprecava da program "pukne", vec kaze ako naidjes na odredjenu vrstu greske uradi to sto pise u bloku
            print("Molimo unesite vreme projekcije u trazenom formatu.")
    return vreme #odmah vraca vrednost iz f-je

def izaberite_salu():
    '''
    Funkcija za odabir sale.
    Vraca odabranu salu, kao i sve njene vrednosti,
    kao sto su broj slobodnih i ukupnih mesta.
    '''
    with open("../data/sale.txt", "r") as f:
        linija = f.read()
        sale = eval(linija)

        while True:
            try:
                print("Izaberite jednu od ponudjenih sala: ")
                for sala in range(1, len(sale)+1):
                    print(str(sala) + " - " + sale[sala-1]["ime_sale"])
                izbor = int(input("Molimo izaberite opciju: "))
                if izbor == 0:
                    stanje = True
                    while stanje:
                        izbor = int(input("Molimo izaberite jednu od postojecih opcija: "))
                        if izbor != 0:
                            stanje = False
                sala = sale[izbor - 1]["ime_sale"]
                break
            except ValueError:
                print("Molimo unesite broj koji se nalazi ispred odredjene sale.")
            except IndexError:
                print("Molimo izaberite jednu od postojecih sala.")
        #print(sala)
        return sala


def izaberite_film():
    '''
    Funkcija ucitava i ispisuje sve filmove.
    Nudi mogucnost dodavanja novog filma.
    '''
    while True:
        with open("../data/filmovi.txt", "r") as f:
            linija = f.read()
            filmovi = eval(linija)
            izbor = None
        try:
            print("Izaberite broj ispred jedne od ponudjenih opcija: ")
            for film in range(1, len(filmovi)+1):
                print(str(film) + " Ime filma: " + filmovi[film - 1]["ime"] + " | Zanr: " + filmovi[film - 1]["zanr"] + 
                    " | Trajanje: " + filmovi[film - 1]["duzina"] + " minuta")
            print(str(film + 1) + " Unos novog filma")
            izbor = int(input("Molimo izaberite opciju: "))
            if izbor == 0:
                stanje = True
                while stanje:
                    izbor = int(input("Molimo izaberite jednu od postojecih opcija: "))
                    if izbor != 0:
                        stanje = False
            film = filmovi[izbor - 1]
            break
        except ValueError:
            print("Molimo unesite broj koji se nalazi ispred odredjenog filma.")
        except IndexError:
            if izbor == len(filmovi) + 1:
                unos_filma()
            else:
                print("Izabrali ste nepostojeci film. Ukoliko zelite da dodate novi film pritisnite " + str(
                    len(filmovi) + 1))
    return film

def provera_poklapanja_projekcija_izmena_projekcije(datum, vreme, sala, duzina, id):
    '''
    Proverava da li je uneta projekcija u konfliktu sa postojecom po pitanju datuma i sale.
    Potom proverava da li se pocetak unete projekcije desava za vreme trajanje postojece,
    kao i da li se kraj unete projekcije desava u vreme kada je vec postojeca projekcija u toku.
    '''
    with open("../data/projekcije.txt", "r") as f:
        linija = f.read()
        projekcije = eval(linija)
        #print(projekcije)

    vreme_unete_projekcije  = vreme.split()
    pocetak_unete_projekcije = datetime.timedelta(hours = int(vreme_unete_projekcije[0]), minutes = int(vreme_unete_projekcije[1]))
    kraj_unete_projekcije = pocetak_unete_projekcije + datetime.timedelta(minutes = int(duzina))
    for projekcija in projekcije:
        #print(projekcija)
        if id == projekcija["id"]:
            continue
        if sala == projekcija["sala"] and datum == projekcija["datum"] and id != projekcija["id"] and projekcija["vidljivost"] == False:
            vreme_postojece_projekcije = projekcija["vreme"].split()
            #print(projekcija["vreme"])
            pocetak_postojece_projekcije = datetime.timedelta(hours = int(vreme_postojece_projekcije[0]), minutes = int(vreme_postojece_projekcije[1]))
            kraj_postojece_projekcije = pocetak_postojece_projekcije + datetime.timedelta(minutes = int(projekcija["duzina"]))
            #if pocetak_unete_projekcije <= pocetak_postojece_projekcije and kraj_unete_projekcije >= pocetak_postojece_projekcije:
            #    return True
            #elif pocetak_unete_projekcije <= kraj_postojece_projekcije and kraj_unete_projekcije >= kraj_postojece_projekcije:
            #    return True
            if pocetak_unete_projekcije < pocetak_postojece_projekcije and kraj_unete_projekcije >= pocetak_postojece_projekcije:
                print("\n" + "Uneta projekcija| " + "pocetak: " + str(pocetak_unete_projekcije) + " kraj: " + str(
                    kraj_unete_projekcije) + " sala: " + sala + "\n" +
                      "Postojeca projekcija| " + "naziv filma: " + projekcija["film"] + " pocetak: " +
                      str(pocetak_postojece_projekcije) + " kraj: " + str(kraj_postojece_projekcije) +
                      " sala: " + projekcija["sala"] + "\n")
                return True
            elif pocetak_unete_projekcije <= kraj_postojece_projekcije and kraj_unete_projekcije > kraj_postojece_projekcije:
                print("\n" + "Uneta projekcija| " + "pocetak: " + str(pocetak_unete_projekcije) + " kraj: " + str(
                    kraj_unete_projekcije) + " sala: " + sala + "\n" +
                      "Postojeca projekcija| " + "naziv filma: " + projekcija["film"] + " pocetak: " +
                      str(pocetak_postojece_projekcije) + " kraj: " + str(kraj_postojece_projekcije) +
                      " sala: " + projekcija["sala"] + "\n")
                return True
            elif pocetak_unete_projekcije >= pocetak_postojece_projekcije and kraj_unete_projekcije <= kraj_postojece_projekcije:
                print("\n" + "Uneta projekcija| " + "pocetak: " + str(pocetak_unete_projekcije) + " kraj: " + str(
                    kraj_unete_projekcije) + " sala: " + sala + "\n" +
                      "Postojeca projekcija| " + "naziv filma: " + projekcija["film"] + " pocetak: " +
                      str(pocetak_postojece_projekcije) + " kraj: " + str(kraj_postojece_projekcije) +
                      " sala: " + projekcija["sala"] + "\n")
                return True
    return False

def provera_poklapanja_projekcija(datum, vreme, sala, duzina):
    '''
    Proverava da li je uneta projekcija u konfliktu sa postojecom po pitanju datuma i sale.
    Potom proverava da li se pocetak unete projekcije desava za vreme trajanje postojece,
    kao i da li se kraj unete projekcije desava u vreme kada je vec postojeca projekcija u toku.
    '''
    with open("../data/projekcije.txt", "r") as f:
        linija = f.read()
        projekcije = eval(linija)
        #print(projekcije)

    vreme_unete_projekcije  = vreme.split()
    pocetak_unete_projekcije = datetime.timedelta(hours = int(vreme_unete_projekcije[0]), minutes = int(vreme_unete_projekcije[1]))
    kraj_unete_projekcije = pocetak_unete_projekcije + datetime.timedelta(minutes = int(duzina))
    for projekcija in projekcije:
        #print(projekcija)
        if sala == projekcija["sala"] and datum == projekcija["datum"] and projekcija["vidljivost"] == False:
            vreme_postojece_projekcije = projekcija["vreme"].split()
            #print(projekcija["vreme"])
            pocetak_postojece_projekcije = datetime.timedelta(hours = int(vreme_postojece_projekcije[0]), minutes = int(vreme_postojece_projekcije[1]))
            kraj_postojece_projekcije = pocetak_postojece_projekcije + datetime.timedelta(minutes = int(projekcija["duzina"]))
            #if pocetak_unete_projekcije <= pocetak_postojece_projekcije and kraj_unete_projekcije >= pocetak_postojece_projekcije:
            #    return True
            #elif pocetak_unete_projekcije <= kraj_postojece_projekcije and kraj_unete_projekcije >= kraj_postojece_projekcije:
            #    return True
            if pocetak_unete_projekcije < pocetak_postojece_projekcije and kraj_unete_projekcije >= pocetak_postojece_projekcije:
                print("\n" + "Nova projekcija| " + "pocetak: " + str(pocetak_unete_projekcije) + " kraj: " + str(
                    kraj_unete_projekcije) + " sala: " + sala + "\n" +
                      "Postojeca projekcija| " + "naziv filma: " + projekcija["film"] + " pocetak: " +
                      str(pocetak_postojece_projekcije) + " kraj: " + str(kraj_postojece_projekcije) +
                      " sala: " + projekcija["sala"] + "\n")
                return True
            elif pocetak_unete_projekcije <= kraj_postojece_projekcije and kraj_unete_projekcije > kraj_postojece_projekcije:
                print("\n" + "Nova projekcija| " + "pocetak: " + str(pocetak_unete_projekcije) + " kraj: " + str(
                    kraj_unete_projekcije) + " sala: " + sala + "\n" +
                      "Postojeca projekcija| " + "naziv filma: " + projekcija["film"] + " pocetak: " +
                      str(pocetak_postojece_projekcije) + " kraj: " + str(kraj_postojece_projekcije) +
                      " sala: " + projekcija["sala"] + "\n")
                return True
            elif pocetak_unete_projekcije >= pocetak_postojece_projekcije and kraj_unete_projekcije <= kraj_postojece_projekcije:
                print("\n" + "Nova projekcija| " + "pocetak: " + str(pocetak_unete_projekcije) + " kraj: " + str(
                    kraj_unete_projekcije) + " sala: " + sala + "\n" +
                      "Postojeca projekcija| " + "naziv filma: " + projekcija["film"] + " pocetak: " +
                      str(pocetak_postojece_projekcije) + " kraj: " + str(kraj_postojece_projekcije) +
                      " sala: " + projekcija["sala"] + "\n")
                return True
    return False


def unos_projekcije():
    '''
    Nakon svih potrebnih provera unosa, upisuje projekciju u vec postojecu listu projekcija.
    Stampa korisniku unetu projekciju.
    '''
    with open("../data/projekcije.txt", "r") as f:
        linija = f.read()
        projekcije = eval(linija)

    print("Unos nove projekcije: ")
    id = str(len(projekcije)+1)
    film = izaberite_film() # parametar film dobija vracenu vrednost iz f-je izaberite_film
    duzina = str(film["duzina"])
    #print(projekcije)
    #print(film)
    while True:
        datum = unesite_datum().strftime("%d %m %Y")
        vreme = unesite_vreme().strftime("%H %M")
        datum = str(datum)
        vreme = str(vreme)
        print("Molimo odaberite salu za odrzavanje projekcije.")
        sala = izaberite_salu() #parametar sala dobija vracenu vrednost iz f-je izaberite_salu
        if provera_poklapanja_projekcija(datum, vreme, sala, duzina):
            print("Molimo unesite projekciju u drugom terminu ili drugoj sali.")
        else:
            break
    while True:
        cena = unos_broja("Molimo unesite cenu u dinarima: ")
        if cena < 100:
            print("Minimalna cena je 100 dinara.")
        else:
            cena = str(cena)
            break
    with open("../data/sale.txt", "r") as f:
        linija = f.read()
        sale = eval(linija)
        #print(sale)
        for i in sale:
            if sala == i["ime_sale"]:
                broj_ukupnih_mesta = str(i["kapacitet"])
                broj_slobodnih_mesta = str(i["kapacitet"])
    nova_projekcija = {"id": id, "datum": datum, "vreme": vreme, "duzina": duzina, "cena": cena, "film": film["ime"], "zanr": film["zanr"], "sala": sala, "vidljivost": False, "broj_ukupnih_mesta": broj_ukupnih_mesta, "broj_slobodnih_mesta": broj_slobodnih_mesta}


    with open("../data/projekcije.txt", "w") as q:
        projekcije.append(nova_projekcija)
        q.write(str(projekcije))
        print("Dodata je nova projekcija.")


    id_nove_projekcije = int(nova_projekcija["id"])-1
    print("""
Uneli ste projekciju: 
Datum: {datum}
Vreme: {vreme}
Duzina: {duzina}
Film: {film}
Zanr: {zanr}
Sala: {sala}
Cena: {cena}
Broj ukupnih mesta: {broj_ukupnih_mesta}
Broj slobodnih mesta: {broj_slobodnih_mesta}
""".format(**projekcije[id_nove_projekcije]))


def pretraga_projekcija(): #funkcija za pretragu projekcije
    '''
    Funkcija za pretragu projekcija.
    Pretrazuje projekcije po id-ju, nazivu filma, 
    zanru filma i sali prikazivanja.
    '''
    while True:
        with open("../data/projekcije.txt", "r") as f:
            linija = f.read()
            projekcije = eval(linija)
            #print(projekcije)

        print("Odaberite po cemu zelite da pretrazite projekcije: ")
        print("1 - ID-u projekcije")
        print("2 - Nazivu filma")
        print("3 - Zanru filma")
        print("4 - Sali prikazivanja")
        print("0 - Povratak na prethodni meni")

        odabir = unos_broja("Molimo izaberite jednu od ponudjenih opcija: ")
        if odabir == 0:
            break


        elif odabir == 1:
            print("Odabrali ste pretragu po ID.")
            pronadjene_projekcije = 0
            zeljeni_ID = str(unos_broja("Molimo unesite zeljeni ID: "))
            for projekcija in range(len(projekcije)):
                if zeljeni_ID == projekcije[projekcija]["id"]:
                    print(projekcije[projekcija]["film"] + " | " + projekcije[projekcija]["datum"] + " u " + projekcije[projekcija]["vreme"])
                    pronadjene_projekcije += 1
                    if projekcije[projekcija]["vidljivost"] == True:
                        print("Projekcija nije aktivna." + "\n")
                    if projekcije[projekcija]["vidljivost"] == False:
                        print("\n")
            if pronadjene_projekcije == 0:
                print("Trenutno nema projekcija sa trazenim ID.")


        elif odabir == 2:
            print("Odabrali ste pretragu po nazivu filma.")
            pronadjene_projekcije = 0
            naziv_filma = input("Molimo unesite naziv filma po kojem zelite da se pretraga izvrsi: ")
            naziv_filma_odvojeno = naziv_filma.split()
            naziv_filma_spojeno = naziv_filma.replace(" ", "" )

            for projekcija in range(len(projekcije)):

                if naziv_filma.lower() == projekcije[projekcija]["film"].lower():
                    print(projekcije[projekcija]["film"] + " | " + str(projekcije[projekcija]["datum"]) + " u " + str(projekcije[projekcija]["vreme"]))
                    pronadjene_projekcije += 1
                    if projekcije[projekcija]["vidljivost"] == True:
                        print("Projekcija nije aktivna." + "\n")
                    if projekcije[projekcija]["vidljivost"] == False:
                        print("\n")
                elif naziv_filma_spojeno.lower() == projekcije[projekcija]["film"].replace(" ", "" ).lower():
                    print(projekcije[projekcija]["film"] + " | " + str(projekcije[projekcija]["datum"]) + " u " + str(projekcije[projekcija]["vreme"]))
                    pronadjene_projekcije += 1
                    if projekcije[projekcija]["vidljivost"] == True:
                        print("Projekcija nije aktivna." + "\n")
                    if projekcije[projekcija]["vidljivost"] == False:
                        print("\n")
                else:
                    for rec in range(len(naziv_filma_odvojeno)):
                        if naziv_filma_odvojeno[rec].lower() in projekcije[projekcija]["film"].lower():
                            print(projekcije[projekcija]["film"] + " | " + projekcije[projekcija]["datum"] + " u " + projekcije[projekcija]["vreme"])
                            pronadjene_projekcije += 1
                            if projekcije[projekcija]["vidljivost"] == True:
                                print("Projekcija nije aktivna." + "\n")
                            if projekcije[projekcija]["vidljivost"] == False:
                                print("\n")
            if pronadjene_projekcije == 0:
                print("Trenutno nema projekcija sa trazenim nazivom filma.")


        elif odabir == 3:
            print("Odabrali ste pretragu po zanru filma.")
            pronadjene_projekcije = 0
            zanr_filma = input("Molimo unesite zanr filma po kojem zelite da se pretraga izvrsi: ")
            zanr_filma = zanr_filma.lower()

            for projekcija in range(len(projekcije)):

                if zanr_filma == projekcije[projekcija]["zanr"]:
                    print(projekcije[projekcija]["film"] + " | " + str(projekcije[projekcija]["datum"]) + " u " + str(
                        projekcije[projekcija]["vreme"]))
                    pronadjene_projekcije += 1
                    if projekcije[projekcija]["vidljivost"] == True:
                        print("Projekcija nije aktivna." + "\n")
                    if projekcije[projekcija]["vidljivost"] == False:
                        print("\n")
                elif zanr_filma == projekcije[projekcija]["zanr"].replace("-", ""):
                    print(projekcije[projekcija]["film"] + " | " + str(projekcije[projekcija]["datum"]) + " u " + str(
                        projekcije[projekcija]["vreme"]))
                    pronadjene_projekcije += 1
                    if projekcije[projekcija]["vidljivost"] == True:
                        print("Projekcija nije aktivna." + "\n")
                    if projekcije[projekcija]["vidljivost"] == False:
                        print("\n")
                elif zanr_filma.replace(" ", "") == projekcije[projekcija]["zanr"].replace("-", ""):
                    print(projekcije[projekcija]["film"] + " | " + str(projekcije[projekcija]["datum"]) + " u " + str(
                        projekcije[projekcija]["vreme"]))
                    pronadjene_projekcije += 1
                    if projekcije[projekcija]["vidljivost"] == True:
                        print("Projekcija nije aktivna." + "\n")
                    if projekcije[projekcija]["vidljivost"] == False:
                        print("\n")
            if pronadjene_projekcije == 0:
                print("Trenutno nema projekcija sa trazenim zanrom filma.")


        elif odabir == 4:
            print("Odabrali ste pretragu po sali projekcije.")
            pronadjene_projekcije = 0

            with open("../data/sale.txt", "r") as p:
                linija = p.read()
                sale = eval(linija)
            print("Postojece sale su: ")
            for sala in range(len(sale)):
                print("Sala: " + sale[sala]["ime_sale"])

            sala_projekcije = input("Molimo unesite salu projekcije po kojem zelite da se pretraga izvrsi: ")
            sala_projekcije = sala_projekcije.lower()

            for projekcija in range(len(projekcije)):

                if sala_projekcije == projekcije[projekcija]["sala"].lower():
                    print(projekcije[projekcija]["film"] + " | " + str(projekcije[projekcija]["datum"]) + " u " + str(
                        projekcije[projekcija]["vreme"]))
                    pronadjene_projekcije += 1
                    if projekcije[projekcija]["vidljivost"] == True:
                        print("Projekcija nije aktivna." + "\n")
                    if projekcije[projekcija]["vidljivost"] == False:
                        print("\n")

            if pronadjene_projekcije == 0:
                print("Trenutno nema projekcija u trazenoj sali ili odabrana sala ne postoji.")

    
def brisanje_projekcije():
    '''
    Funkcija za brisanje projekcija.
    Pretrazuje projekcije koje nisu obrisane i brise onu koju korisnik odabere.
    '''
    with open("../data/projekcije.txt", "r") as f:
        linija = f.read()
        projekcije = eval(linija)

        stanje = True
        brojac_aktivnih_projekcija = 0

        aktivni_id = []
        print("Izabrali ste opciju za brisanje projekcije.")
        for projekcija in range(len(projekcije)):

            if projekcije[projekcija]["vidljivost"] == False:
                print(projekcije[projekcija]["id"] + " - " + projekcije[projekcija]["film"] + " | " +
                      projekcije[projekcija]["datum"] + " u " + projekcije[projekcija]["vreme"])
                aktivni_id.append(projekcije[projekcija]["id"])
                brojac_aktivnih_projekcija += 1
        print("0 - Povratak na prethodni meni")
        print("\n")
        print("Ukupan broj projekcija je: ", len(projekcije))
        print("Broj aktivnih projekcija je: ", brojac_aktivnih_projekcija)
        print("Aktivni id-ijevi su: ", aktivni_id)

        if brojac_aktivnih_projekcija == 0:
            print("Trenutno nema aktivnih projekcija.")


        if brojac_aktivnih_projekcija != 0:
            brojac_projekcija = brojac_aktivnih_projekcija
            while stanje:
                id_projekcije = unos_broja("Molimo unesite broj koji se nalazi ispred zeljene opcije: ")

                if id_projekcije == 0:
                    break

                if str(id_projekcije) in aktivni_id:

                    for projekcija in range(len(projekcije)):

                        if str(id_projekcije) == projekcije[projekcija]["id"] and projekcije[projekcija]["vidljivost"] == False:
                            with open("../data/projekcije.txt", "w") as q:
                                projekcije[projekcija]["vidljivost"] = True
                                q.write(str(projekcije))
                                print("Zeljena projekcija je obrisana.")
                                brojac_projekcija -= 1
                                stanje = False

                if brojac_projekcija == 0:
                    print("Nema vise aktivnih projekcija.")
                    break


def izmena_projekcije(): #funkcija za izmenu projekcije
    '''
    Funkcija za izmenu projekcije.
    Omogucava korisniku da menja datum, vreme, salu i cenu postojecih projekcija.
    '''
    while True:
        print("Odaberite id projekcije koju zelite da menjate.")
        with open("../data/projekcije.txt", "r") as f:
            linija = f.read()
            projekcije = eval(linija)
            #print(projekcije)

        for projekcija in projekcije:
            print("{id} | {film} | {datum} | {vreme} | {sala}".format(**projekcija))
        print("0 - Povratak na prethodni meni")
        izbor = None
        while True:
            try:
                unos = unos_broja("Unesite id projekcije koju zelite da menjate: ")-1
                if unos == -1:
                    izbor = unos
                    break
                projekcija = projekcije[unos]
                izbor = unos
                break
            except IndexError:
                print("Molimo unesite jednu od ponudjenih opcija.")
        if izbor <= -1:
            break
        if projekcije[izbor]["vidljivost"] == True:
            print("Odabrana projekcija nije aktivna.")
            break
        print("Odaberite sta biste hteli da promenite kod izabrane projekcije.")
        print("1 - Datum: " + projekcija["datum"])
        print("2 - Vreme: " + projekcija["vreme"])
        print("3 - Sala: " + projekcija["sala"])
        print("4 - Cena: " + projekcija["cena"])
        print("0 - Povratak na prethodni meni.")
        unos = unos_broja("Molimo odaberite jednu od opcija: ")

        while True:
            if unos == 1:
                while True:
                    promenjen_datum = unesite_datum().strftime("%d %m %Y")
                    if provera_poklapanja_projekcija_izmena_projekcije(promenjen_datum, projekcija["vreme"], projekcija["sala"],
                                                     projekcija["duzina"], projekcija["id"]):
                        print("Molimo unesite projekciju u drugom terminu ili drugoj sali.")
                    else:
                        with open("../data/projekcije.txt", "w") as q:
                            projekcija["datum"] = promenjen_datum
                            q.write(str(projekcije))
                            print("Datum zeljene projekcije je promenjen.")
                        break
                break
            elif unos == 2:
                #print(projekcija)
                while True:
                    promenjeno_vreme = unesite_vreme().strftime("%H %M")

                    if provera_poklapanja_projekcija_izmena_projekcije(projekcija["datum"], promenjeno_vreme, projekcija["sala"],
                                                    projekcija["duzina"], projekcija["id"]):
                        print("Molimo unesite projekciju u drugom terminu ili drugoj sali.")
                    else:
                        with open("../data/projekcije.txt", "w") as q:
                            projekcija["vreme"] = promenjeno_vreme
                            q.write(str(projekcije))
                            print("Vreme zeljene projekcije je promenjeno.")
                        break
                break
            elif unos == 3:
                while True:
                    promenjena_sala = izaberite_salu()
                    if provera_poklapanja_projekcija_izmena_projekcije(projekcija["datum"], projekcija["vreme"], promenjena_sala,
                                                     projekcija["duzina"], projekcija["id"]):
                        print("Molimo unesite projekciju u drugom terminu ili drugoj sali.")
                    else:
                        with open("../data/projekcije.txt", "w") as q:
                            projekcija["sala"] = promenjena_sala
                            q.write(str(projekcije))
                            print("Sala zeljene projekcije je promenjena.")
                        break
                break
            elif unos == 4:
                with open("../data/projekcije.txt", "w") as q:
                    projekcija["cena"] = str(unos_dinara("Molimo unesite cenu u dinarima: "))
                    q.write(str(projekcije))
                    print("Cena zeljene projekcije je promenjena.")
                break
            elif unos == 0:
                break
            else:
                print("Izabrali ste nepostojecu opciju.")
                break
#trostrukim navodnicima """ se omogucava ispis teksta kako je on sam i napisan