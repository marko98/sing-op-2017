neomogucena_opcija = "Izabrana opcija trenutno nije omogucena."
nijedna_od_opcija = "Izaberite jednu od ponudjenih opcija."

def glavniMeni(uloga):#proverava ulogu korisnika
    if uloga == "menadzer":#proverava da li je uloga korisnika menadzer
        print("Vase mogucnosti su: ") #ispisuje ono sto se nalazi u zagradi (stringove ako su pod navodnicima ili vrednosti nekih promenljivih)
        print("0 - Log out")
        print("1 - Pretraga projekcija")
        print("2 - Unos nove projekcije")
        print("3 - Brisanje projekcije")
        print("4 - Izmena projekcije")
        print("5 - Unos novog prodavca")
        print("6 - Brisanje prodavca")
        while True: #petlja koja omogucava nasem programu da vrsi nas kod dokle god je nesto True
            izbor = int(input("Odaberite opciju: "))
            if izbor == 0: #ukoliko je izbor 0, uradi to sto pise u bloku
                print("Izabrali ste opciju za logout")
                break #prekida petlju
            elif izbor == 1: #ukoliko je izbor 1, uradi to sto pise u bloku
                print (neomogucena_opcija)
            elif izbor == 2: #ukoliko je izbor 2, uradi to sto pise u bloku
                print (neomogucena_opcija)
            elif izbor == 3: #ukoliko je izbor 3, uradi to sto pise u bloku
                print (neomogucena_opcija)
            elif izbor == 4: #ukoliko je izbor 4, uradi to sto pise u bloku
                print (neomogucena_opcija)
            elif izbor == 5: #ukoliko je izbor 5, uradi to sto pise u bloku
                print (neomogucena_opcija)
            elif izbor == 6: #ukoliko je izbor 6, uradi to sto pise u bloku
                print (neomogucena_opcija)
            else: #ukoliko izbor nije nista od navedenog, uradi to sto pise u bloku
                print(nijedna_od_opcija)

    else: #"govori" sta da se izvrsi ukoliko uloga korisnika nije menadzer
        print("0 - Log out")
        print("1 - Pretraga projekcija")
        print("2 - Prodaja karata")
        while True: #petlja koja omogucava nasem programu da vrsi nas kod dokle god je nesto True
            izbor = int(input("Odaberite opciju: "))
            if izbor == 0: #ukoliko je izbor 0, uradi to sto pise u bloku
                print("Izabrali ste opciju za logout")
                break #prekida petlju
            elif izbor == 1: #ukoliko je izbor 1, uradi to sto pise u bloku
                print (neomogucena_opcija)
            elif izbor == 2: #ukoliko je izbor 2, uradi to sto pise u bloku
                print (neomogucena_opcija)
            else: #ukoliko izbor nije nista od navedenog, uradi to sto pise u bloku
                print(nijedna_od_opcija)