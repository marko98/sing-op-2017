def dodavanje_prodavca():
    '''
    Funkcija pomocu koje menadzer dodaje novog prodavca.
    Podaci se ucitavaju, na njih se dodaje novi korisnik cija je uloga prodavac, zatim se upisuje u tekstualni fajl.
    '''
    with open("../data/korisnici.txt", "r") as f:
        linija = f.read()
        korisnici = eval(linija)
        #print(korisnici)

        korisnickoime = ""

    while korisnickoime == "":
        korisnickoime = input("Molimo unesite korisnicko ime prodavca: ")
        for korisnik in korisnici:
            #print(korisnik)
            if korisnik["korisnickoime"] == korisnickoime:
                korisnickoime = ""
                print("Uneto korisnicko ime je zauzeto, probajte sa nekim drugim.")

    korisnickoime = korisnickoime.capitalize()
    ime = ""
    prezime = ""
    sifra = ""

    while ime == "" or prezime == "":
        ime = input("Molimo unesite ime prodavca: ")
        prezime = input("Molimo unesite prezime prodavca: ")
        ime = ime.capitalize()
        prezime = prezime.capitalize()

        if True != ime.isalpha():
            ime = ""
        if True != prezime.isalpha():
            prezime = ""
        if True != ime.isalpha() or True != prezime.isalpha():
            print("Molimo unesite pravo ime/prezime prodavca.")

    while sifra == "":
       sifra = input("Molimo unesite sifru prodavca: ")

    novi_prodavac = dict()
    novi_prodavac["korisnickoime"] = korisnickoime
    novi_prodavac["sifra"] = sifra
    novi_prodavac["ime"] = ime
    novi_prodavac["prezime"] = prezime
    novi_prodavac["uloga"] = "prodavac"
    novi_prodavac["vidljivost"] = False

    with open("../data/korisnici.txt", "w") as q:
        korisnici.append(novi_prodavac)
        q.write(str(korisnici))
        print("Prodavac je uspesno dodat.")

def brisanje_prodavca():
    '''
    Funkcija pomocu koje menadzer brise zeljenog prodavca.
    Podaci se ucitavaju, nad njima se vrse odredjene promene, a zatim se upisuju u tekstualni fajl.
    '''

    with open("../data/korisnici.txt", "r") as f:
        linija = f.read()
        korisnici = eval(linija)
        #print(korisnici)

        stanje = True
        brojac = 0

        for korisnik in korisnici:
            #print(korisnik)

            if korisnik["uloga"] == "prodavac" and korisnik["vidljivost"] == False:
                print("Ime prodavca: " + korisnik["ime"] + "\n" + "Prezime prodavca: " + korisnik["prezime"] + "\n" +
                      "Korisnicko ime prodavca: " + korisnik["korisnickoime"] + "\n" + "Sifra prodavca: " +
                      korisnik["sifra"] + "\n")
                brojac += 1
        #print(brojac)

        if brojac != 0:
            while stanje:
                postoji_korisnik = True
                korisnickoime = input("Molimo unesite korisnicko ime prodavca kojeg zelite da obrisete: ")
                for korisnik in korisnici:
                    if korisnik["korisnickoime"] == korisnickoime:
                        postoji_korisnik = False
                    if korisnik["uloga"] == "prodavac" and korisnik["vidljivost"] == False and korisnik[
                        "korisnickoime"] == korisnickoime:
                        with open("../data/korisnici.txt", "w") as q:
                            korisnik["vidljivost"] = True
                            korisnik["korisnickoime"] = "Korisnik je obrisan"
                            q.write(str(korisnici))
                            print("Prodavac je izbrisan.")
                        brojac -= 1
                        stanje = False
                #print(brojac)
                if postoji_korisnik == True:
                    print("Korisnik sa unetim korisnickim imenom ne postoji.")

        if brojac == 0:
            print("Trenutno nema vise aktivnih prodavaca.")