from korisnici import login #importujemo funkciju login iz modula korisnici
from projekcije import * #importujemo sve iz modula projekcije
from prodavci import * #importujemo sve iz modula projekcije
from racun import * #importujemo sve iz modula racun


neomogucena_opcija = "Izabrana opcija trenutno nije omogucena."
nijedna_od_opcija = "Izaberite jednu od ponudjenih opcija."

def main(): #poziva funkciju glavni meni, zbog while petlje, f-ja ce se stalno pozivati
    while True: #petlja koja omogucava nasem programu da vrsi nas kod dokle god je nesto True
        glavni_meni(login()) #poziva f-ju login iz modula korisnici, a potom dobijenu vrednost prosledjuje f-ji glavniMeni

def glavni_meni(uloga): #proverava ulogu korisnika
    if uloga == "menadzer": #proverava da li je uloga korisnika menadzer
        while True: #petlja koja omogucava nasem programu da vrsi nas kod dokle god je nesto True
            print("Vase mogucnosti su: ") #ispisuje ono sto se nalazi u zagradi (stringove ako su pod navodnicima ili vrednosti nekih promenljivih)
            print("0 - Log out")
            print("1 - Pretraga projekcija")
            print("2 - Unos nove projekcije")
            print("3 - Brisanje projekcije")
            print("4 - Izmena projekcije")
            print("5 - Unos novog prodavca")
            print("6 - Brisanje prodavca")

            try: #try i except - sintaksa blokova za obradu greske
                izbor = int(input("Odaberite opciju: ")) #odabir jedne od ponudjenih opcija
                if izbor == 0: #ukoliko je izbor 0, uradi to sto pise u bloku
                    break #prekida petlju
                elif izbor == 1: #ukoliko je izbor 1, uradi to sto pise u bloku
                    pretraga_projekcija() #poziva odredjenu funkciju iz modula projekcije
                elif izbor == 2: #ukoliko je izbor 2, uradi to sto pise u bloku
                    unos_projekcije() #poziva odredjenu funkciju iz modula projekcije
                elif izbor == 3: #ukoliko je izbor 3, uradi to sto pise u bloku
                    brisanje_projekcije() #poziva odredjenu funkciju iz modula projekcije
                elif izbor == 4: #ukoliko je izbor 4, uradi to sto pise u bloku
                    izmena_projekcije() #poziva odredjenu funkciju iz modula projekcije
                elif izbor == 5: #ukoliko je izbor 5, uradi to sto pise u bloku
                    dodavanje_prodavca() #poziva odredjenu funkciju iz modula projekcije
                elif izbor == 6: #ukoliko je izbor 6, uradi to sto pise u bloku
                    brisanje_prodavca() #poziva odredjenu funkciju iz modula projekcije
                else: #ukoliko izbor nije nista od navedenog, uradi to sto pise u bloku
                    print(nijedna_od_opcija)
            except ValueError: #sprecava da program "pukne", vec kaze ako naidjes na odredjenu vrstu greske uradi to sto pise u bloku
                print(nijedna_od_opcija)

    else: #"govori" sta da se izvrsi ukoliko uloga korisnika nije menadzer
        while True: #petlja koja omogucava nasem programu da vrsi nas kod dokle god je nesto True
            print("0 - Log out")
            print("1 - Pretraga projekcija")
            print("2 - Prodaja karata")
            try: #try i except - sintaksa blokova za obradu greske
                izbor = int(input("Odaberite opciju: ")) #odabir jedne od ponudjenih opcija
                if izbor == 0: #ukoliko je izbor 0, uradi to sto pise u bloku
                    break #prekida petlju
                elif izbor == 1: #ukoliko je izbor 1, uradi to sto pise u bloku
                    pretraga_projekcija() #poziva odredjenu funkciju iz modula projekcije
                elif izbor == 2: #ukoliko je izbor 2, uradi to sto pise u bloku
                    prodaja_karata() #poziva odredjenu funkciju iz modula racun
                else: #ukoliko izbor nije nista od navedenog, uradi to sto pise u bloku
                    print(nijedna_od_opcija)
            except ValueError: #sprecava da program "pukne", vec kaze ako naidjes na odredjenu vrstu greske uradi to sto pise u bloku
                print(nijedna_od_opcija)


main() #poziva f-ju main